import shutil
import tempfile

import dramatiq
import factory
import pytest
from aioresponses import aioresponses
from django.db.backends.base.base import BaseDatabaseWrapper
from dynamic_preferences.registries import global_preferences_registry
from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.auth.models import AnonymousUser
from pytest_django.plugin import _blocking_manager


def pytest_configure(config):
    """
    Allows plugins and conftest files to perform initial configuration.
    This hook is called for every plugin and initial conftest
    file after command line options have been parsed.
    """
    # register an additional marker
    config.addinivalue_line(
        "markers", "with_fe: mark test to run only when explicitly indicating the presence of a collected front-end"
    )


def pytest_runtest_setup(item):
    env_with_fe = [mark for mark in item.iter_markers(name="with_fe")]
    if env_with_fe:
        pytest.skip("test requires the presence of a collected front-end")

    from django.db import models
    from olki.apps.federation.keys import get_key_pair
    from olki.apps.federation.models import Actor
    args = Actor.get_actor_data('olki')
    try:
        Actor.objects.get(fid=args.get('fid'))
    except models.ObjectDoesNotExist:
        private, public = get_key_pair()
        args["private_key"] = private.decode("utf-8")
        args["public_key"] = public.decode("utf-8")
        args["type"] = "Application"
        Actor.objects.create(**args)


_blocking_manager.unblock()
_blocking_manager._blocking_wrapper = BaseDatabaseWrapper.ensure_connection


@pytest.fixture(scope="session", autouse=True)
def factories_autodiscover():
    from django.apps import apps
    from olki.fixtures import factories

    app_names = [app.name for app in apps.app_configs.values()]
    factories.registry.autodiscover(app_names)


@pytest.fixture
def factories(db):
    """
    Returns a dictionary containing all registered factories with keys such as
    account.User or corpus.Corpus
    """
    from olki.fixtures import factories

    for v in factories.registry.values():
        try:
            v._meta.strategy = factory.CREATE_STRATEGY
        except AttributeError:
            # probably not a class based factory
            pass
    yield factories.registry


@pytest.fixture
def fixtures(request, factories):
    """
    Inject class attributes per function.

    Arguments of the current function are defined in olki/conftest.py
    and generally injected in all functions, but not classes (hence
    the current fixture designed for Django's TestCase).
    """
    request.cls.factories = factories


@pytest.fixture
def nodb_factories():
    """
    Returns a dictionnary containing all registered factories with a build strategy
    that does not require access to the database
    """
    from olki.fixtures import factories

    for v in factories.registry.values():
        try:
            v._meta.strategy = factory.BUILD_STRATEGY
        except AttributeError:
            # probably not a class based factory
            pass
    yield factories.registry


@pytest.fixture
def preferences(db, cache):
    """
    return a dynamic_preferences manager for global_preferences
    """
    manager = global_preferences_registry.manager()
    manager.all()
    yield manager


@pytest.fixture
def tmpdir():
    """
    Returns a temporary directory path where you can write things during your
    test
    """
    d = tempfile.mkdtemp()
    yield d
    shutil.rmtree(d)


@pytest.fixture
def tmpfile():
    """
    Returns a temporary file where you can write things during your test
    """
    yield tempfile.NamedTemporaryFile()


@pytest.fixture(autouse=True)
def rsa_small_key(settings):
    # smaller size for faster generation, since it's CPU hungry
    settings.RSA_KEY_SIZE = 512


@pytest.fixture(autouse=True)
def a_responses():
    with aioresponses() as m:
        yield m


@pytest.fixture(autouse=True)
def r_mock(requests_mock):
    """
    Returns a requests_mock.mock() object you can use to mock HTTP calls made
    using python-requests
    """
    yield requests_mock


@pytest.fixture
def authenticated_actor(factories, mocker):
    """
    Returns an authenticated ActivityPub actor
    """
    actor = factories["federation.Actor"]()
    mocker.patch(
        "olki.apps.federation.authentication.SignatureAuthentication.authenticate_actor",
        return_value=actor,
    )
    yield actor


@pytest.fixture
def broker():
    broker = dramatiq.get_broker()
    broker.flush_all()
    return broker


@pytest.fixture
def worker(broker):
    worker = dramatiq.Worker(broker, worker_timeout=100)
    worker.start()
    yield worker
    worker.stop()


@pytest.fixture
def rq_authenticated(rf, factories):
    user = factories['account.User']()

    request = rf.post('/')
    SessionMiddleware().process_request(request)
    request.session.save()
    request.user = user

    return request


@pytest.fixture
def rq_anonymous(rf):
    request = rf.post('/')
    SessionMiddleware().process_request(request)
    request.session.save()
    request.user = AnonymousUser()

    return request
