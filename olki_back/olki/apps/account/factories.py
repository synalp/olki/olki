import factory
from faker import Faker
from rolepermissions.roles import assign_role

from olki.fixtures.factories import registry, NoUpdateOnCreate

fake = Faker()


@registry.register
class UserFactory(NoUpdateOnCreate, factory.django.DjangoModelFactory):
    class Meta:
        model = "account.User"

    is_active = True

    email = factory.Faker("email")
    username = factory.Faker("user_name")

    @factory.post_generation
    def post_save(self, create, extracted, **kwargs):
        assign_role(self, fake.random_element(elements=[
            'untrusted_user',
            'trusted_user'
        ]))
        self.set_actor(self.username)
