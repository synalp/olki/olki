from django.core.management.base import BaseCommand
from django_subcommands import SubCommands
from rolepermissions.roles import remove_role, assign_role


class AddCommand(BaseCommand):
    help = 'Add a new local user. By default sends a mail to ask to activate the account.'

    def add_arguments(self, parser):
        parser.add_argument('email', type=str)
        parser.add_argument('password', type=str)

    def handle(self, *args, **options):
        user = UserModel.objects.create(
            email=options['email']
        )
        user.set_password(options['password'])
        user.save()
        self.stdout.write(self.style.SUCCESS('Successfully created user {}'.format(options['email'])))


class ModCommand(BaseCommand):
    help = 'Modify a local user.'

    def add_arguments(self, parser):
        parser.add_argument('--id', type=str, required=False)
        parser.add_argument('--email', type=str, required=False)
        parser.add_argument('--username', type=str, required=False)
        parser.add_argument(
            '--role',
            type=str,
            help='Modify the role of the user to the given role, stripping the user of rights if the new role is below the current role.',
        )

    def handle(self, *args, **options):
        params = {key: value for (key, value) in options.items() if value is not None and key in ['id', 'email', 'username']}
        if hasattr(params, 'username'):
            params['actor__username'] = params['username']
            del params['username']
        user = UserModel.objects.get(**params)

        if options['role'] and options['role'] in ['admin', 'administrator', 'mod', 'moderator', 'user']:
            if options['role'] == 'admin' or options['role'] == 'administrator':
                user.is_staff = True
                user.is_superuser = True
                assign_role(user, 'administrator')
            if options['role'] == 'mod' or options['role'] == 'moderator':
                user.is_staff = True
                user.is_superuser = False
                remove_role(user, 'administrator')
                assign_role(user, 'moderator')
            if options['role'] == 'user':
                user.is_staff = False
                user.is_superuser = False
                remove_role(user, 'moderator')
                remove_role(user, 'administrator')
            user.save()
        else:
            return

        self.stdout.write(self.style.SUCCESS(
            'Successfully modified user {} to be {}'.format(
                options['email'],
                options['role']
            )
        ))


class DetailCommand(BaseCommand):
    help = 'Details of a local user.'

    def add_arguments(self, parser):
        parser.add_argument('--id', type=str, required=False)
        parser.add_argument('--email', type=str, required=False)
        parser.add_argument('--username', type=str, required=False)

    def handle(self, *args, **options):
        params = {key: value for (key, value) in options.items() if value is not None and key in ['id', 'email', 'username']}
        if hasattr(params, 'username'):
            params['actor__username'] = params['username']
            del params['username']
        user = UserModel.objects.get(**params)
        import json
        raise NotImplementedError
        #print(json.dumps(user.to_json(), sort_keys=False, indent=6))


from tabulate import tabulate
from django.utils import formats
from django.core.management.base import BaseCommand
from django.template.defaultfilters import filesizeformat

from olki.apps.account.models import User as UserModel


class ListCommand(BaseCommand):
    help = 'Lists local user accounts.'

    def handle(self, *args, **options):
        users = [
            [
                user.email,
                formats.date_format(user.created_at, "SHORT_DATETIME_FORMAT"),
                user.is_active,
                user.roles,
                (user.actor.username if hasattr(user, 'actor') else ''),
                len(user.actor.corpora_owned.all()),
                filesizeformat(sum([corpus.filesize for corpus in user.actor.corpora_owned.all()]))
            ]
            for user in UserModel.objects.all()
        ]
        print(tabulate(users, headers=['Email', 'Created At', 'Activated', 'Roles', 'Actor', 'Corpora', 'Size on disk']))


class Command(SubCommands):
    subcommands = {
        "mod": ModCommand,
        "add": AddCommand,
        "list": ListCommand,
        "detail": DetailCommand
    }
