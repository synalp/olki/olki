import pytest

from django.conf import settings
from django.contrib.auth import get_user_model, authenticate
from django.test import TestCase, override_settings


@pytest.mark.django_db
class BackendsTestCase(TestCase):
    UserModel = get_user_model()
    user_credentials = {'email': 'test@example.com', 'password': 'test'}

    def setUp(self):
        self.user = self.UserModel.objects.create(
            **self.user_credentials
        )
        self.superuser = self.UserModel.objects.create_superuser(
            username='test2',
            email='test2@example.com',
            password='test',
        )

    #@override_settings(AUTHENTICATION_BACKENDS=[
       #"olki.apps.account.backends.OlkiLDAPBackend",
    #   "olki.apps.account.backends.OlkiAuthBackend",
    #])
    def test_auth_backends(self):
        #self.assertEqual([
           #"olki.apps.account.backends.OlkiLDAPBackend",
        #   "olki.apps.account.backends.OlkiAuthBackend",
        #], settings.AUTHENTICATION_BACKENDS)
        self.assertEqual(self.user, self.UserModel.objects.get(email='test@example.com'))
        #self.assertEqual(self.user, authenticate(email='test@example.com', password='test'))
