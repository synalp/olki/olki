import pytest
from olki.schema import schema


@pytest.mark.django_db(transaction=True)
def test_create_note(rq_authenticated, factories):
    user = factories['account.User']()
    corpus = factories['corpus.Corpus'](actor=user.actor)
    query = """
    mutation createNote {
      createNote(input:{
        pk: """ + '"{}"'.format(corpus.pk) + """
        inReplyTo: ""
        name: "test"
        content: "test"
      }) {
        note {
          name
          content
        }
      }
    }
    """
    expected = {
        "createNote": {
            "note": {
                "name": "test",
                "content": "test"
            }
        }
    }

    result = schema.execute(query, context_value=rq_authenticated)
    assert not result.errors
    assert result.data == expected
