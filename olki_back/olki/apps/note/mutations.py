import graphene
from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator
from graphene.types import ID
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.types import ErrorType
from graphql_jwt.decorators import login_required

from olki.apps.corpus.models import Corpus as CorpusModel
from olki.apps.federation.routes import outbox
from .models import Note as NoteModel


class NoteForm(forms.ModelForm):
    class Meta:
        model = NoteModel
        fields = (
            'name',
            'content',
            'tags',
        )

    language = forms.CharField(required=False)


class CreateNote(DjangoModelFormMutation):
    class Input:
        # TODO: make 'pk' optional to allow posts rather than just replies
        pk = ID(required=True, description='A Flax ID that is used as a primary key for the Corpus model the Note references.')
        inReplyTo = graphene.String(required=True)

    class Meta:
        form_class = NoteForm
        exclude_fields = ('id', )

    @classmethod
    @login_required
    def mutate_and_get_payload(cls, root, info, **input):
        form = cls.get_form(root, info, **input)
        pk = input.get('pk')
        in_reply_to = input.get('inReplyTo')

        if form.is_valid():
            # get corpus
            corpus = CorpusModel.objects.get(id=pk)

            # create Note
            obj = NoteModel(
                **form.cleaned_data,
                actor=info.context.user.actor,
                corpus=corpus
            )
            try:
                URLValidator()(in_reply_to)
                obj.inReplyTo = in_reply_to
            except ValidationError:
                pass
            finally:
                obj.save()

            # create Activity holding the note, and dispatch it
            outbox.dispatch({"type": "Create", "object": {"type": "Note"}}, context={"note": obj})

            kwargs = {cls._meta.return_field_name: obj}
            return cls(errors=[], **kwargs)
        else:
            errors = [
                ErrorType(field=key, messages=value)
                for key, value in form.errors.items()
            ]

            return cls(errors=errors)


class DeleteNote(graphene.Mutation):
    """
    Mutation to delete a Note. It doesn't delete the Note object, but
    rather switches its visibility to False
    """
    class Arguments:
        pk = ID(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @login_required
    def mutate(self, info, pk):
        try:
            obj = NoteModel.objects.get(id=pk)
        except NoteModel.DoesNotExist:
            return DeleteNote(
                success=False,
                errors=['Note matching the provided key could not be found']
            )
        obj.public = False
        obj.save(update_fields=['public'])
        return DeleteNote(
            success=True,
            errors=None
        )


class NoteMutations(graphene.AbstractType):
    createNote = CreateNote.Field()
    deleteNote = DeleteNote.Field()
