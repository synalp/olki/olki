from django.urls import path

from . import views


urlpatterns = [
    # syndication feeds
    path('rss.xml', views.rss(), name='rss'),
    path('atom.xml', views.atom(), name='atom'),

    # ex: /corpus/7C3RSC5fS1n30uTJ.zip
    path('<str:corpus_id>.zip', views.zip, name='archive-zip'),
    path('<str:corpus_id>.tar', views.tar, name='archive-tar'),

    # citations files
    path('<str:corpus_id>.bib', views.bib, name='citation-bib'),
    path('<str:corpus_id>.ris', views.ris, name='citation-ris'),
    path('<str:corpus_id>.endnote', views.endnote, name='citation-endnote'),
    path('<str:corpus_id>.dc', views.dublin_core, name='citation-dublin_core'),
]
