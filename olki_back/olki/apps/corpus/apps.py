from django.apps import AppConfig


class AppConfig(AppConfig):
    name = 'olki.apps.corpus'

    def ready(self):
        pass
