from django.conf import settings
from django.urls import reverse
from rest_framework import serializers

from olki.apps.core.utils import FrozenDict
from . import models


class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Corpus
        fields = [
            "id",
            "title",
            "authors",
            "description",
            "language",
            "created_at",
            "original_publication_at",
        ]


class DublinCoreRecordSerializer(RecordSerializer):
    def to_representation(self, instance):
        data = {
            FrozenDict({
                "_name": "oai_dc:dc",
                "xmlns:oai_dc": "http://www.openarchives.org/OAI/2.0/oai_dc/",
                "xmlns:dc": "http://purl.org/dc/elements/1.1/",
                "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
                "xmlns:tei": "http://www.tei-c.org/ns/1.0",
                "xsi:schemaLocation": "http://www.openarchives.org/OAI/2.0/oai_dc/  http://www.openarchives.org/OAI/2.0/oai_dc.xsd http://purl.org/dc/elements/1.1/  http://dublincore.org/schemas/xmls/qdc/2008/02/11/dc.xsd",
            }): {
                FrozenDict({
                    "_name": "dc:title",
                    "xml:lang": instance.language
                }): instance.title,
                "dc:description": instance.description,
                **{
                    FrozenDict({
                        "_name": "dc:creator",
                        "_id": 'authors' + str(index)
                    }): author.name
                    for index, author in enumerate(instance.authors.all() or [])
                },
                **{
                    FrozenDict({
                        "_name": "dc:creator",
                        "_id": 'owners' + str(index)
                    }): actor.name
                    for index, actor in enumerate([instance.actor])
                },
                "dc:identifier": instance.id,
                "dc:date": str((instance.original_publication_at or instance.created_at).replace(microsecond=0).isoformat()).replace('+00:00', 'Z'),
                "dc:language": instance.language
            }
        }
        if self.context.get('metadata_enclosed', False):
            data = {
                FrozenDict({
                    "_name": "metadata",
                    "xmlns:oai_dc": "http://www.openarchives.org/OAI/2.0/oai_dc/",
                    "xmlns:dc": "http://purl.org/dc/elements/1.1/"
                }): data
            }
        return data


class BibTeXSerializer(RecordSerializer):
    def to_representation(self, instance):
        data = {
            "author": ", ".join([author.name for author in instance.authors.all()]),
            "publisher": settings.SITE_NAME,
            "title": instance.title,
            "year": (instance.original_publication_at or instance.created_at).year,
            "url": settings.INSTANCE_URL + reverse(
                "corpus",
                urlconf="olki.apps.{}.spa_urls".format(instance._meta.app_label),
                kwargs={"corpus_id": instance.id}
            )
        }
        return data


class RISSerializer(RecordSerializer):
    def to_representation(self, instance):
        data = {
            "AU": instance.authors.first() if instance.authors.exists() else None,
            "PB": settings.SITE_NAME,
            "TI": instance.title,
            "PY": (instance.original_publication_at or instance.created_at).year,
            "UR": settings.INSTANCE_URL + reverse(
                "corpus",
                urlconf="olki.apps.{}.spa_urls".format(instance._meta.app_label),
                kwargs={"corpus_id": instance.id}
            )
        }
        return data


class EndNoteSerializer(RecordSerializer):
    def to_representation(self, instance):
        data = {
            "authors": instance.authors.first() if instance.authors.exists() else None,
            "title": instance.title,
            "year": (instance.original_publication_at or instance.created_at).year,
            "url": settings.INSTANCE_URL + reverse(
                "corpus",
                urlconf="olki.apps.{}.spa_urls".format(instance._meta.app_label),
                kwargs={"corpus_id": instance.id}
            ),
            "language": instance.language
        }
        return data
