import pytest
import json
from olki.schema import schema


def escape(value) -> str:
    return json.dumps(json.dumps(value))


@pytest.mark.django_db(transaction=True)
def test_create_simple_corpus(rq_authenticated):
    query = """
    mutation createCorpus {
      createCorpus(input:{
        title: "test",
        description: "test",
        language: "en",
        license: "cc-0"
      }) {
        corpus {
          title
          description
          license
          language
        }
      }
    }
    """
    expected = {
        "createCorpus": {
            "corpus": {
                "title": "test",
                "description": "test",
                "license": "cc-0",
                "language": "en"
            }
        }
    }

    result = schema.execute(query, context_value=rq_authenticated)
    assert not result.errors
    assert result.data == expected


@pytest.mark.django_db(transaction=True)
def test_create_complex_corpus(rq_authenticated):
    related = escape([{
        "link": "https://hal.archives-ouvertes.fr/hal-01181142",
        "relation": "isCitedBy"
    }])
    authors = escape([
        {
            "name": "John Doe",
            "affiliation": "LORIA",
            "role": "Contact person"
        },
        {
            "name": "Jane Doe",
            "affiliation": "INRIA",
            "role": "Contact person"
        }
    ])
    tags = escape([
        {
            "name": "Science",
            "id": "wikidata.org/entity/Q336",
            "description": "study and knowledge of the natural world"
        },
        {
            "code": "wikidata.org/entity/Q10870555",
            "name": "Report",
            "description": "Formal, detailed text"
        }
    ])

    query = """
    mutation createCorpus {
      createCorpus(input:{
        title: "test",
        description: "test",
        language: "en",
        license: "cc-0",
        digitalObjectIdentifier: "",
        draft: false,
        originalPublicationAt: "2020-05-12T00:00:00",
        related: %s,
        authors: %s,
        tags: %s,
        commentsAllowed: true
      }) {
        corpus {
          title
          description
          license
          language
          related
          authors {
            email
            affiliation
            role
            name
            orcid
          }
          tags {
            code
            name
            description
          }
          draft
          commentsAllowed
        }
      }
    }
    """ % (
        related,
        authors,
        tags
    )
    result = schema.execute(query, context_value=rq_authenticated)
    assert not result.errors
    corpus = result.data['createCorpus']['corpus']
    assert len(corpus['tags']) > 0
    assert len(corpus['authors']) > 0
    assert len(corpus['related']) > 0
    assert corpus['draft'] is False
    assert corpus['commentsAllowed'] is True


@pytest.mark.django_db(transaction=True)
def test_edit_simple_corpus(rq_authenticated, factories):
    corpus = factories['corpus.Corpus']()
    query = """
    mutation editCorpus {
      editCorpus(input:{
        pk: """ + '"{}"'.format(corpus.pk) + """
        title: "test",
        description: "test",
        language: "en",
        license: "cc-0"
      }) {
        corpus {
          title
          description
          license
          language
        }
      }
    }
    """

    result = schema.execute(query, context_value=rq_authenticated)
    assert not result.errors
