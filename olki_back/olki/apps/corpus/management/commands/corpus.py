from django.core.management.base import BaseCommand
from django_subcommands import SubCommands
from django.utils import formats
from tabulate import tabulate
from django.template.defaultfilters import filesizeformat

from olki.apps.corpus.models import Corpus as CorpusModel


class ListCommand(BaseCommand):
    help = 'Lists known corpora.'

    def handle(self, *args, **options):
        users = [
            [
                corpus.id,
                corpus.actor.full_username,
                formats.date_format(corpus.created_at, "SHORT_DATETIME_FORMAT"),
                corpus.draft,
                corpus.quarantined,
                filesizeformat(corpus.filesize),
                corpus.title
            ]
            for corpus in CorpusModel.objects.all()
        ]
        print(tabulate(users, headers=['Id', 'Actor', 'Created At', 'Draft', 'Quarantined', 'Size on disk', 'Title']))


class DetailCommand(BaseCommand):
    help = 'Details of a corpus.'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('corpus', type=str)

    def handle(self, *args, **options):
        corpus = CorpusModel.objects.get(
            id=options["corpus"]
        )
        import json
        print(json.dumps(corpus.to_json(), sort_keys=False, indent=6))


class Command(SubCommands):
    subcommands = {
        "list": ListCommand,
        "detail": DetailCommand,
    }
