import functools
import operator

from django.contrib.postgres.search import (
    SearchRank,
    SearchVector,
    TrigramSimilarity,
    TrigramDistance
)
from django.db import models
from django.db.models import F, Q
from django.db.models.functions import Greatest


class CorpusManager(models.Manager):
    search_vectors = (
            SearchVector('title', weight='A', config='english') +
            SearchVector('description', weight='D', config='english')
    )

    def create(self, *args, **kwargs):
        return super(CorpusManager, self).create(*args, **kwargs)

    def search(self, text):
        similarity = Greatest(
            TrigramSimilarity('title', text),
            TrigramSimilarity('description', text),
        )
        distance = Greatest(
            TrigramDistance('title', text),
            TrigramDistance('description', text),
        )
        rank = SearchRank(F('search_vector'), text)
        return self.get_queryset()\
            .annotate(rank=rank)\
            .annotate(similarity=similarity)\
            .annotate(distance=distance)\
            .filter(
                Q(similarity__gte=0.03) if len(text) > 5 else Q()
                &
                Q(distance__lte=0.5) if len(text) > 5 else Q()
                &
                functools.reduce(operator.and_, (Q(search_vector__icontains=x) for x in text.split()))
            )\
            .order_by('distance')
