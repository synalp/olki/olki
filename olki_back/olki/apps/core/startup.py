from django.apps import AppConfig


class AppConfig(AppConfig):
    """
    Override this method in subclasses to run code when Django starts.

    This code will be run once at startup
    """
    name = 'olki.apps.core'

    def ready(self):
        pass  # startup code here
