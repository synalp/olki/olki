import random
import requests
from django.core.management.base import BaseCommand
from django.apps import apps
from olki.fixtures import factories
from olki.apps.oaipmh.tasks import clone_record_oaipmh
from olki.apps.account.models import User as UserModel

app_names = [app.name for app in apps.app_configs.values()]
factories.registry.autodiscover(app_names)
f = factories.registry


class Command(BaseCommand):
    help = 'Seeds the database.'

    def add_arguments(self, parser):
        parser.add_argument('--users',
                            default=5,
                            type=int,
                            help='The number of fake users to create.')
        parser.add_argument('--corpora',
                            default=20,
                            type=int,
                            help='The number of fake corpora to create.')
        parser.add_argument('--import',
                            default="rigelk",
                            type=str,
                            help='Import corpora from HAL instead of generating them.')

    def handle(self, *args, **options):
        if options['import']:
            r = requests.get("https://api.archives-ouvertes.fr/search/hal?q=*&wt=json&rows={}".format(options["corpora"]))
            docids = [doc.get('uri_s').split("/")[3] for doc in r.json()['response']['docs']]
            for id in docids:
                clone_record_oaipmh.send_with_options(args=("HAL", "https://api.archives-ouvertes.fr/oai/hal/", "oai:HAL:{}".format(id)),
                                                      kwargs={
                                                          'user_id': UserModel.objects.get(username=options['import']).id,
                                                          'comments_allowed': True
                                                      })
        else:
            return
            users = [f['account.User']() for _ in range(options['users'])]
            for _ in range(options['corpora']):
                user = users[random.randint(0, options['users']-1)]
