import logging

from django.apps import apps
from django.urls import resolve
from spa.middleware import SPAMiddleware

logger = logging.getLogger(__name__)


class HeaderMiddleware(SPAMiddleware):
    def __call__(self, request):
        # Code to be executed for each request before
        # the later middleware are called.
        response = self.process_request(request)
        if response is None:
            response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.
        for app in [app.name for app in apps.app_configs.values() if 'olki.apps' in app.name]:
            try:
                match = resolve(request.path, urlconf='{}.spa_urls'.format(app))
                modified_response = match.func(request, response, *match.args, **match.kwargs)
                if modified_response:
                    return modified_response
            except ImportError as e:
                pass
            except Exception as e:
                logger.debug(e)

        return response
