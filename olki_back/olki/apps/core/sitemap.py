from django.contrib.sitemaps import Sitemap
from django.contrib.sites.models import Site
from django.db.utils import settings

from olki.apps.corpus.models import Corpus
from olki.apps.account.models import User


class SitemapMixin(Sitemap):
    protocol = 'https'

    def get_urls(self, page=1, site=None, protocol=None):
        # see https://github.com/django/django/blob/2.2.0/django/contrib/sitemaps/__init__.py
        site = Site(domain=settings.DOMAIN)
        return super(SitemapMixin, self).get_urls(page, site, protocol=None)


class StaticViewSitemap(SitemapMixin):
    changefreq = 'monthly'
    priority = 0.3

    def items(self):
        return [
            'about',
        ]

    def location(self, item):
        return '/{}'.format(item)


class CorpusSitemap(SitemapMixin):
    changefreq = 'weekly'
    priority = 0.8

    def items(self):
        return Corpus.public.all()[:40000]

    def lastmod(self, obj):
        return obj.updated_at


class UserSitemap(SitemapMixin):
    changefreq = 'weekly'
    prority = 0.2

    def items(self):
        return User.objects\
            .exclude(actor__isnull=True)\
            .exclude(profile__indexed=False)\
            .order_by('id')[:9000]


sitemaps = {
    'static': StaticViewSitemap,
    'corpora': CorpusSitemap,
    'users': UserSitemap
}
