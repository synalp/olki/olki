import logging
from promise import is_thenable

logger = logging.getLogger(__name__)


class DebugMiddleware(object):
    def on_error(self, error):
        logger.error(error)

    def resolve(self, next, root, info, *args, **kwargs):
        if root is None:
            # This will only be called once for a request
            if info.parent_type.__str__() in ['Queries', 'Mutations']:
                logger.info("{operation}".format(
                    operation=info.operation.selection_set.selections[0].name.value
                ))

        result = next(root, info, *args, **kwargs)
        if is_thenable(result):
            result.catch(self.on_error)
        return result


def debug(next, root, info, *args, **kwargs):
    """Debug GraphQL middleware.
    For more information read:
    https://docs.graphene-python.org/en/latest/execution/middleware/#middleware
    """
    # Skip Graphiql introspection requests, there are a lot.
    if (
        info.operation.name is not None
        and info.operation.name.value != "IntrospectionQuery"
    ):
        print("Debug middleware report")
        print("    operation :", info.operation.operation)
        print("    name      :", info.operation.name.value)

    # Invoke next middleware.
    return next(root, info, *args, **kwargs)
