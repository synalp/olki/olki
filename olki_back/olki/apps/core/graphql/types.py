import graphene
from django.db import models
from graphene_django.converter import convert_django_field


class CountableConnectionBase(graphene.Connection):
    """Connection subclass that supports totalCount."""

    class Meta:
        abstract = True

    total_count = graphene.Int()

    def resolve_total_count(self, info, **kwargs):
        if isinstance(self.iterable, models.query.QuerySet):
            return self.iterable.count()
        return len(self.iterable)


class URI(graphene.Scalar):
    """An RFC 3986, RFC 3987, and RFC 6570 (level 4) compliant URI string."""

    @classmethod
    def serialize(cls, value):
        return value


@convert_django_field.register(models.URLField)
def convert_field_to_uri(field, registry=None):
    return graphene.Field(
        URI,
        description=field.help_text,
        required=not field.null
    )
