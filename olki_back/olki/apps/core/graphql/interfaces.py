from graphene import ID, String
from graphene.types import Interface


class AuthorInterface(Interface):
    name = String(required=True)
    orcid = String()


class ActionTargetInterface(Interface):
    pk = ID()
    title = String(required=True)
