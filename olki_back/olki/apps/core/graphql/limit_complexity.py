from django.conf import settings
from graphene.types.definitions import GrapheneGraphQLType

from olki.apps.core.graphql.utils.fragments import get_fields


def measure_depth(info):

    def dict_depth(d, level=1):
        if not isinstance(d, dict) or not d:
            return level
        return max(dict_depth(d[k], level + 1) for k in d)

    return dict_depth(get_fields(info))


class DepthAnalysisMiddleware(object):
    """
    Middleware that parses requests, analyses queries only,
    and checks the depth of the query by unfolding fragments
    if necessary.

    Usage:
    # in settings.py:
    GRAPHENE = {
        'MIDDLEWARE': [
            'olki.apps.core.graphql.limit_complexity.DepthAnalysisMiddleware'
        ],
        'QUERY_DEPTH_LIMIT': 8,
    }
    """
    def resolve(self, next, root, info, **args):
        if info.operation.operation == 'query' and (info.path[0] not in ['__schema', '__introspection']):
            depth = measure_depth(info)
            if depth > settings.GRAPHENE.get('QUERY_DEPTH_LIMIT'):
                raise Exception('Query is too complex (depth).')
        return next(root, info, **args)


class CostAnalysisMiddleware(object):
    """
    Middleware that parses requests, analyses queries only,
    and checks the cost of the query by relying on provided
    information by the model. In the absence of a cost attribute,
    a default cost of 1 is given.

    Usage:
    # in settings.py:
    GRAPHENE = {
        'MIDDLEWARE': [
            'olki.apps.core.graphql.limit_complexity.CostAnalysisMiddleware'
        ],
        'QUERY_COST_LIMIT': 1000,
    }

    # in your class:
    class MyClass(DjangoObjectType):
        cost = 1

        class Meta:
            …
    """
    class ResolveCounter:
        def __init__(self):
            from threading import Lock
            self.count = 0
            self.max = settings.GRAPHENE.get('QUERY_COST_LIMIT')
            self.lock = Lock()

        def increment(self, cost=1):
            with self.lock:
                if self.count < self.max:
                    self.count += cost
                else:
                    raise Exception('Query is too complex (cost).')

    def resolve(self, next, root, info, **args):
        # graphene_type holds all variables of your DjangoObjectType
        graphene_type = getattr(info.return_type, 'graphene_type', None)

        # inject the 'resolve_cost_counter' attribute if first call to resolve()
        context = info.context
        if not hasattr(context, 'resolve_cost_counter'):
            setattr(context, 'resolve_cost_counter', self.ResolveCounter())
        # unfolding with new context
        result = next(root, info, **args)

        # exception for the retrieving the schema (to let the API self-document)
        # as blocking it is the role of a dedicated middleware.
        if info.path[0] not in ['__schema', '__introspection']:
            if not isinstance(info.return_type, GrapheneGraphQLType):
                getattr(context, 'resolve_cost_counter').increment()
                return result

            cost = getattr(graphene_type, 'cost', None)
            if not cost:
                info.context.resolve_cost_counter.increment()
                return result

            getattr(context, 'resolve_cost_counter').increment(cost=cost)

        return result
