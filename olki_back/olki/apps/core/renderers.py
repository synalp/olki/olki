import json
from io import StringIO

from django.utils.encoding import smart_text
from django.utils.xmlutils import SimplerXMLGenerator
from rest_framework import renderers
from rest_framework_xml.renderers import XMLRenderer


class OlkiJSONRenderer(renderers.JSONRenderer):
    charset = 'utf-8'
    object_label = 'object'
    pagination_object_label = 'objects'
    pagination_object_count = 'count'

    def render(self, data, accepted_media_type=None, renderer_context=None):
        del accepted_media_type
        if data.get('results', None) is not None:
            return json.dumps({
                self.pagination_object_label: data['results']
            })

        # If the view throws an error (such as the user can't be authenticated
        # or something similar), `data` will contain an `errors` key. We want
        # the default JSONRenderer to handle rendering errors, so we need to
        # check for this case.
        if data.get('errors', None) is not None:
            return super(OlkiJSONRenderer, self).render(data)

        return json.dumps({
            self.object_label: data
        })


class OLKiXMLRenderer(XMLRenderer):
    item_tag_name = "list-item"
    root_tag_name = "root"

    def render(self, data, accepted_media_type=None, renderer_context=None, without_root=False):
        """
        Renders `data` into serialized XML.
        """
        if data is None:
            return ''

        stream = StringIO()

        xml = SimplerXMLGenerator(stream, self.charset)
        if not without_root:
            xml.startDocument()
            xml.startElement(self.root_tag_name, {})

        self._to_xml(xml, data)

        if not without_root:
            xml.endElement(self.root_tag_name)
            xml.endDocument()
        return stream.getvalue()

    def _to_xml(self, xml, data):
        if isinstance(data, (list, tuple)):
            for item in data:
                xml.startElement(self.item_tag_name, {})
                self._to_xml(xml, item)
                xml.endElement(self.item_tag_name)

        elif isinstance(data, dict):
            for key, value in data.items():
                token = key
                attributes = {}
                if isinstance(key, dict):
                    token = key['_name']
                    attributes = {_key: value for _key, value in key.items() if _key not in ['_name', '_id']}

                xml.startElement(token, {**attributes})
                self._to_xml(xml, value)
                xml.endElement(token)

        elif data is None:
            # Don't output any value
            pass

        else:
            xml.characters(smart_text(data))
