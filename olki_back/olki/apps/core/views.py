import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.http.response import JsonResponse, HttpResponse, HttpResponseRedirect
from django.utils.timezone import now
from dynamic_preferences.registries import global_preferences_registry

from olki import __version__
from olki.apps.corpus.models import Corpus
from olki.apps.federation.models import Activity, Domain
from .utils import generate_nodeinfo_metadata


def generate_usage_statistics():
    return {
        "users": {
            "total": get_user_model().objects.count(),
            "activeHalfyear": get_user_model().objects.filter(
                last_login__gte=now() - datetime.timedelta(days=180)).count(),
            "activeMonth": get_user_model().objects.filter(
                last_login__gte=now() - datetime.timedelta(days=30)).count(),
        },
        "localPosts": Corpus.local.count(),
        "localComments": Activity.objects.filter(Q(payload__type="Create") & Q(
            actor__domain=Domain.objects.get_or_create(name=settings.INSTANCE_HOSTNAME)[0]) & Q(
            payload__object__type='Note') & Q(object_id__isnull=False)).count(),
    }


def nodeinfo1_view(request):
    """Generate a NodeInfo 1.0 document."""
    global_preferences = global_preferences_registry.manager()
    usage = {"users": {}}
    data = {
        "version": "1.0",
        "software": {
            "name": "olki",
            "version": __version__
        },
        "protocols": {"inbound": ["activitypub"], "outbound": ["activitypub"]},
        "services": {
            "inbound": [
                "oaipmh",
            ],
            "outbound": [
                "oaipmh",
                "atom1.0",
                "rss2.0",
            ]
        },
        "openRegistrations": global_preferences['general__registration_allowed'],
        "usage": usage,
        "metadata": generate_nodeinfo_metadata()
    }
    if not settings.INSTANCE_REDUCED_STATISTICS:
        data.update({"usage": generate_usage_statistics()})
    return JsonResponse(data)


def nodeinfo2_view(request):
    """Generate a NodeInfo 2.0 document."""
    global_preferences = global_preferences_registry.manager()
    usage = {"users": {}}
    data = {
        "version": "2.0",
        "software": {
            "name": "olki",
            "version": __version__
        },
        "protocols": ["activitypub"],
        "services": {
            "inbound": [
                "oaipmh",
            ],
            "outbound": [
                "oaipmh",
                "atom1.0",
                "rss2.0",
            ]
        },
        "openRegistrations": global_preferences['general__registration_allowed'],
        "usage": usage,
        "metadata": generate_nodeinfo_metadata()
    }
    if not settings.INSTANCE_REDUCED_STATISTICS:
        data.update({"usage": generate_usage_statistics()})
    return JsonResponse(data)


def nodeinfo21_view(request):
    """Generate a NodeInfo 2.1 document."""
    global_preferences = global_preferences_registry.manager()
    usage = {"users": {}}
    data = {
        "version": "2.1",
        "software": {
            "name": "olki",
            "version": __version__,
            "repository": "https://framagit.org/synalp/olki/olki",
            "homepage": "https://olki.loria.fr/platform/"
        },
        "protocols": ["activitypub"],
        "services": {
            "inbound": [
                "oaipmh",
            ],
            "outbound": [
                "oaipmh",
                "atom1.0",
                "rss2.0",
            ]
        },
        "openRegistrations": global_preferences['general__registration_allowed'],
        "usage": usage,
        "metadata": generate_nodeinfo_metadata()
    }
    if not settings.INSTANCE_REDUCED_STATISTICS:
        data.update({"usage": generate_usage_statistics()})
    return JsonResponse(data)


def change_password_view(request):
    return HttpResponseRedirect(redirect_to='/settings')


def host_meta_view(request):
    xml = """<?xml version="1.0" encoding="UTF-8"?>
<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
  <Link rel="lrdd" type="application/xrd+xml" template="{webserver}/.well-known/webfinger?resource={{uri}}"/>
</XRD>""".format(webserver=settings.INSTANCE_URL)
    return HttpResponse(xml, content_type='application/xml')
