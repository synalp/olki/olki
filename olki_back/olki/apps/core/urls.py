from django.urls import path
from django.contrib.sitemaps.views import sitemap


from .views import nodeinfo1_view, nodeinfo2_view, nodeinfo21_view, change_password_view, host_meta_view
from .sitemap import sitemaps

app_name = 'core'
urlpatterns = [
    # nodeinfo
    path('.well-known/nodeinfo/1.0', nodeinfo1_view, name='nodeinfo-1.0'),
    path('.well-known/nodeinfo/2.0', nodeinfo2_view, name='nodeinfo-2.0'),
    path('.well-known/nodeinfo/2.1', nodeinfo21_view, name='nodeinfo-2.1'),

    # change password: https://news.ycombinator.com/item?id=18618193
    path('.well-known/change-password', change_password_view, name='change-password'),

    # host-meta:
    path('.well-known/host-meta', host_meta_view, name='host-meta'),

    # sitemap
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
]
