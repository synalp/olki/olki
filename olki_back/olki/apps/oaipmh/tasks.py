# pylint: skip-file

import logging

import dramatiq
from sickle import Sickle

from olki.apps.account.models import User
from olki.apps.core.utils import require_instance
from olki.apps.corpus.forms import CorpusForm
from olki.apps.oaipmh.serializers import RecordSerializer
from olki.apps.federation.routes import outbox
from olki.apps.federation.schema import OnNewNotificationForUser
from .models import Repository

logger = logging.getLogger(__name__)


def fetch_oai_identify(url):
    sickle = Sickle(url)
    return dict(sickle.Identify())


def fetch_oai_records(repository):
    sickle = Sickle(repository.url)
    records = sickle.ListRecords(
        **{
            'metadataPrefix': 'oai_dc',
            # 'from': 'yyyy-mm-dd',
            'ignore_deleted': True,
        }
    )
    for record in records:
        try:
            s = RecordSerializer(data=record.metadata)
            s.is_valid()
            c = CorpusForm(s.data)
            c.is_valid()
            c.repository = repository
            c.save()
        except Exception as e:
            logging.exception("failed for record {}".format(record.header.identifier))
            continue


def fetch_oai_record(user, repository, oai_parser_url, oai_id, **kwargs):
    sickle = Sickle(repository.url)
    record = sickle.GetRecord(
        **{
            'identifier': oai_id,
            'metadataPrefix': 'oai_dc'
        }
    )
    s = RecordSerializer(data=record.metadata)
    s.is_valid()
    c = CorpusForm(s.data)
    c.is_valid()
    c.repository = repository
    corpus = c.save()
    corpus.actor = user.actor
    corpus.comments_allowed = kwargs.get('comments_allowed', False)
    corpus.save()
    # create actions for the actor involved
    outbox.dispatch({"type": "Create", "object": {"type": "Corpus"}}, context={"corpus": corpus})
    # create notifications for the user involved
    OnNewNotificationForUser.notify_user(user, "{} successfully imported".format(corpus.title), summary="Corpus import finished", url="corpus/{}".format(corpus.pk))


@dramatiq.actor(actor_name="oaipmh.fetch", max_retries=3)
def update_repository_oaipmh(url):
    """
    Given a repository url, fetches the repository
    TODO: test & use?
    """
    r = Repository.objects.update_or_create(url=url)[0]
    if r.status == 'enabled':
        r.oaipmh_identify = fetch_oai_identify(url)
        r.save()
        fetch_oai_records(url, r)


@dramatiq.actor(actor_name="oaipmh.clone", max_retries=0)
@require_instance(User.objects.select_related(), "user")
def clone_record_oaipmh(oai_parser_url, oai_repository_url, oai_id, user=None, **kwargs):
    """
    Given a repository url and a corpus url on that repository, fetches the corpus
    """
    r = Repository.objects.update_or_create(url=oai_repository_url)[0]
    r.oaipmh_identify = fetch_oai_identify(oai_repository_url)
    r.save()
    fetch_oai_record(user, r, oai_parser_url, oai_id, **kwargs)
