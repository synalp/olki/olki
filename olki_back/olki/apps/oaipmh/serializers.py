import json

from rest_framework import serializers


class RecordSerializer(serializers.Serializer):
    creator = serializers.ListField(child=serializers.CharField(), min_length=1)
    date = serializers.ListField(child=serializers.CharField(), min_length=1)
    source = serializers.ListField(child=serializers.CharField(), min_length=1)
    identifier = serializers.ListField(child=serializers.CharField(), min_length=1)
    title = serializers.ListField(child=serializers.CharField(), min_length=1)
    type = serializers.ListField(child=serializers.CharField(), min_length=1)
    subject = serializers.ListField(child=serializers.CharField(), min_length=1)
    language = serializers.ListField(child=serializers.CharField(), min_length=1, required=False)
    abstract = serializers.ListField(child=serializers.CharField(), min_length=1, required=False)
    description = serializers.ListField(child=serializers.CharField(), min_length=1, required=False)
    rights = serializers.ListField(child=serializers.CharField(), min_length=1, required=False)

    def to_representation(self, instance):
        data = super(RecordSerializer, self).to_representation(instance)

        # doi
        doi = None
        for i in data.get('identifier'):
            if i.startswith('DOI: '):
                doi = 'http://dx.doi.org/' + i.lstrip('DOI: ')
                break

        # cloned_from
        cloned_from = None
        for i in data.get('source'):
            if i.startswith('https://'):
                cloned_from = i
                break

        # authors
        authors = [{'name': "{1} {0}".format(*author.split(', '))} for author in data.get('creator') if
                   len(author.split(', ')) == 2]

        # tags
        tags = json.dumps([{'name': subject} for subject in data.get('subject', []) if len(subject) > 3])

        # description
        description = data.get('description') or ''
        if description:
            del description[0]
            description = "\n".join(description)
        if data.get('abstract'):
            description = data.get('abstract') + '\n' + description

        # TODO: original_publication_at

        return {
            'digital_object_identifier': doi,  # a url
            'authors': json.dumps(authors),
            'title': data.get('title')[0],  # a string
            'category': data.get('type')[0],  # a string
            'tags': tags,  # a list of dicts with a name
            'language': data.get('language')[0] if data.get('language', False) else 'en',  # a string, iso639-1 code
            'description': description,  # a string
            # 'original_publication_at': original_publication_at, # a valid date/time
            'cloned_from': cloned_from  # a url
        }
