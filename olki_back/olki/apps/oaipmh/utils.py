try:
    from urllib.parse import urlencode, quote, unquote
except ImportError:
    from urllib import quote, unquote, urlencode
import cgi

from .error import BadResumptionTokenError
from olki.apps.core.datestamp import datetime_to_datestamp, datestamp_to_datetime


def encodeResumptionToken(kw, cursor):
    kw = kw.copy()
    kw['cursor'] = str(cursor)
    from_ = kw.get('from_')
    if from_ is not None:
        kw['from_'] = datetime_to_datestamp(from_)
    until = kw.get('until')
    if until is not None:
        kw['until'] = datetime_to_datestamp(until)
    return quote(urlencode(kw))


def decodeResumptionToken(token):
    token = str(unquote(token))

    try:
        kw = cgi.parse_qs(token, True, True)
    except ValueError:
        raise BadResumptionTokenError(
            "Unable to decode resumption token: %s" % token)
    result = {}
    for key, value in kw.items():
        value = value[0]
        if key == 'from_' or key == 'until':
            value = datestamp_to_datetime(value)
        result[key] = value
    try:
        cursor = int(result.pop('cursor'))
    except (KeyError, ValueError):
        raise BadResumptionTokenError(
            "Unable to decode resumption token (bad cursor): %s" % token)
    # XXX should also validate result contents. Need verb information
    # for this, and somewhat more flexible verb validation support
    return result, cursor
