from django.core.management.base import BaseCommand

from olki.apps.account.models import User
from olki.apps.oaipmh.tasks import clone_record_oaipmh


class Command(BaseCommand):
    help = "Import HAL documents as Corpus objects."

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument(
            'oai_identifiers',
            type=str,
            nargs='?',
            help="HAL ids to import",
        )

    def handle(self, *args, **options):
        oai_identifier = options['oai_identifiers']
        if not oai_identifier:
            self.stdout.write(self.style.ERROR("no oai identifier input (e.g. 'hal-01181142' for https://hal.archives-ouvertes.fr/hal-01181142)"))

        clone_record_oaipmh.send_with_options(args=('HAL', 'https://api.archives-ouvertes.fr/oai/hal/', "oai:HAL:{}".format(oai_identifier)),
                                              kwargs={
                                                  'user_id': User.objects.first().id,
                                                  'comments_allowed': False
                                              })
