from django.apps import AppConfig


class AppConfig(AppConfig):
    name = 'olki.apps.federation'

    def ready(self):
        pass
