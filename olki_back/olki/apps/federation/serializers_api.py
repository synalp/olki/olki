# pylint: skip-file

from rest_framework import serializers

from olki.apps.core import serializers as common_serializers

from . import filters
from . import models


def serialize_generic_relation(activity, obj):
    data = {"uuid": obj.uuid, "type": obj._meta.label}
    return data


class ActivitySerializer(serializers.ModelSerializer):
    from . import serializers as federation_serializers

    actor = federation_serializers.APIActorSerializer()
    object = serializers.SerializerMethodField()
    target = serializers.SerializerMethodField()
    related_object = serializers.SerializerMethodField()

    class Meta:
        model = models.Activity
        fields = [
            "uuid",
            "fid",
            "actor",
            "payload",
            "object",
            "target",
            "related_object",
            "actor",
            "creation_date",
            "type",
        ]

    def get_object(self, o):
        if o.object:
            return serialize_generic_relation(o, o.object)

    def get_related_object(self, o):
        if o.related_object:
            return serialize_generic_relation(o, o.related_object)

    def get_target(self, o):
        if o.target:
            return serialize_generic_relation(o, o.target)


class InboxItemSerializer(serializers.ModelSerializer):
    activity = ActivitySerializer()

    class Meta:
        model = models.InboxItem
        fields = ["id", "type", "activity", "is_read"]
        read_only_fields = ["id", "type", "activity"]


class InboxItemActionSerializer(common_serializers.ActionSerializer):
    actions = [common_serializers.Action("read", allow_all=True)]
    filterset_class = filters.InboxItemFilter

    def handle_read(self, objects):
        return objects.update(is_read=True)


class FetchSerializer(serializers.ModelSerializer):
    from . import serializers as federation_serializers

    actor = federation_serializers.APIActorSerializer()

    class Meta:
        model = models.Fetch
        fields = [
            "id",
            "url",
            "actor",
            "status",
            "detail",
            "creation_date",
            "fetch_date",
        ]
