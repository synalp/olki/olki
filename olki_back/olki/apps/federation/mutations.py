import graphene
from graphene.types import List, String, Boolean, ID, Int
from graphql_jwt.decorators import superuser_required
from graphql import GraphQLError

from .models import InboxItem as InboxItemModel, Actor, Follow


class EditInboxItem(graphene.Mutation):
    """
    Mutation to edit an InboxItem

    Passing a pk edits a specific InboxItem, otherwise edits in bulk all InboxItems!
    """
    class Arguments:
        pk = ID()
        is_read = Boolean(required=True, description='A boolean to change the notification read status.')

    success = Boolean()
    errors = List(String)

    def mutate(self, info, pk, is_read):
        is_authenticated = info.context.user.is_authenticated
        if not is_authenticated:
            errors = ['unauthenticated']
        else:
            if pk:
                try:
                    inbox_item = InboxItemModel.objects.get(id=pk)
                    # if not has_object_permission('modify_inboxitem', info.context.user, inbox_item) and not info.context.user.actor.corpora_owned.filter(pk=inbox_item.pk).exists():
                    #     errors = ['unauthorized to edit InboxItem']
                    #     return EditInboxItem(success=False, errors=errors)
                    inbox_item.is_read = is_read
                    inbox_item.save()
                    return EditInboxItem(success=True)
                except InboxItemModel.DoesNotExist:
                    errors = ['InboxItem not found']
            else:
                InboxItemModel.objects.filter(actor=info.context.user.actor).update(is_read=is_read)
                return EditInboxItem(success=True)
        return EditInboxItem(success=False, errors=errors)


class AddFollow(graphene.Mutation):
    """
    Mutation to add a Follow
    """
    class Arguments:
        domain = String(required=True)
        scan = Boolean(default_value=True)

    success = Boolean()
    errors = List(String)

    @superuser_required
    def mutate(self, info, domain, scan):
        uri = domain + '/federation/actor/olki'
        from .actors import get_actor
        try:
            actor = Actor.objects.get(fid=Actor.get_actor_data('olki').get('fid'))
            remote_actor = get_actor(uri)

            # create follow in db
            follow = Follow.objects.get_or_create(actor=actor, target=remote_actor)[0]

            # dispatch follow to target
            from .routes import outbox
            outbox.dispatch({"type": "Follow"}, context={"follow": follow})
            return AddFollow(success=True, errors=[])
        except Exception as e:
            return GraphQLError(str(e))


class DeleteFollow(graphene.Mutation):
    """
    Mutation to delete a Follow
    """
    class Arguments:
        pk = Int(required=True)

    success = Boolean()
    errors = List(String)

    @superuser_required
    def mutate(self, info, pk):
        try:
            follow = Follow.objects.get(id=pk)
            follow.delete()
            return DeleteFollow(success=True, errors=[])
        except Follow.DoesNotExist:
            return GraphQLError('does not exist')


class FederationMutations(graphene.AbstractType):
    editInboxItem = EditInboxItem.Field()
    addFollow = AddFollow.Field()
    deleteFollow = DeleteFollow.Field()
