from django.core.management.base import BaseCommand
from django_subcommands import SubCommands
from django.db import transaction
from tabulate import tabulate

from olki.apps.federation.models import Actor, Follow
from olki.apps.federation.actors import get_actor
from olki.apps.federation.routes import outbox


class AddCommand(BaseCommand):
    help = 'Follows the given instance(s)/account(s), instance-wide.'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument(
            'uri',
            nargs='+',
            type=str,
            help="domain(s) of the instance(s) to follow, space-separated"
        )

        # Named (optional) arguments
        parser.add_argument(
            '--user',
            type=str,
            help='Make the command relative to the user instead of the instance actor.',
        )

    @transaction.atomic
    def handle(self, *args, **options):
        uris = options['uri']
        actor = Actor.objects.get(fid=Actor.get_actor_data('olki').get('fid'))

        for uri in uris:
            uri = uri + '/federation/actor/olki'
            try:
                remote_actor = get_actor(uri)

                # create follow in db
                follow = Follow.objects.get_or_create(actor=actor, target=remote_actor)[0]

                # dispatch follow to target
                outbox.dispatch({"type": "Follow"}, context={"follow": follow})

                self.stdout.write(self.style.SUCCESS(
                    'Successfully followed {}: {}'.format(uri, remote_actor)
                ))
            except Exception as e:
                self.stderr.write(self.style.ERROR(e))
                raise e


class ListCommand(BaseCommand):
    help = 'Lists the instances/accounts followed by the instance.'

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--user',
            type=str,
            help='Make the command relative to the user instead of the instance actor.',
        )
        parser.add_argument('--outbound', dest='outbound', action='store_true')
        parser.add_argument('--inbound', dest='outbound', action='store_false')
        parser.set_defaults(outbound=True)

    def handle(self, *args, **options):
        if options['user']:
            actor = Actor.objects.get(fid=Actor.get_actor_data(options['user']).get('fid'))
        else:
            actor = Actor.objects.get(fid=Actor.get_actor_data('olki').get('fid'))

        follows = actor.emitted_follows.all()
        if not options['outbound']:
            follows = actor.received_follows.all()

        print(tabulate([
            [
                follow.target.fid if options['outbound'] else follow.actor.fid,
                follow.creation_date,
                follow.approved
            ]
            for follow in follows
        ],
        headers=['Actor', 'Created At', 'Approved']))


class DelCommand(BaseCommand):
    help = 'Deletes the Follows to the given instance(s)/account(s), instance-wide.'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument(
            'uri',
            nargs='+',
            type=str,
            help="URI(s) to the instance(s)/account(s), space-separated"
        )

        # Named (optional) arguments
        parser.add_argument(
            '--user',
            type=str,
            help='Make the command relative to the user instead of the instance actor.',
        )

    def handle(self, *args, **options):
        uris = options['uri']
        instance = Actor.objects.get(fid=Actor.get_actor_data('olki').get('fid'))

        for uri in uris:
            try:
                remote_actor = get_actor(uri)
                outbox.dispatch({"type": "Undo", "object.type": "Follow"}, context={"follow": {
                    "actor": instance,
                    "target": remote_actor
                }})
                self.stdout.write(self.style.SUCCESS(
                    'Successfully unfollowed {}'.format(uri)
                ))
            except Exception as e:
                self.stdout.write(self.style.ERROR(e))


class Command(SubCommands):
    subcommands = {
        "add": AddCommand,
        "del": DelCommand,
        "list": ListCommand,
    }
