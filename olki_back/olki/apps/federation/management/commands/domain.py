from django.core.management.base import BaseCommand
from django_subcommands import SubCommands
from django.conf import settings
from django.core.validators import ValidationError
import argparse
from olki.apps.core.validators import DomainValidator
from olki.apps.core.preferences import get, set

from olki.apps.federation.models import Actor, Domain


def valid_domain(domain):
    _domain = domain.split(':')[0]
    try:
        d = DomainValidator()
        d(_domain)
    except ValidationError:
        raise argparse.ArgumentTypeError("Not a valid domain: '{0}'".format(domain))


class MigrateCommand(BaseCommand):
    help = "Migrate domains. Might cause errors in the past federation relationships."

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument(
            'hostname',
            type=str,
            help="New hostname",
        )
        parser.add_argument(
            'old_hostname',
            type=str,
            help="Old hostname",
            nargs='?',
            default=get('general__site_hostname'),
        )

    def handle(self, *args, **options):
        valid_domain(options['old_hostname'])
        valid_domain(options['hostname'])
        try:
            for actor in Actor.objects.filter(
                domain=Domain.objects.get(
                    name=options["old_hostname"]
                )
            ).iterator():
                actor.migrate_domain(
                    domain=Domain.objects.get_or_create(
                        name=options["hostname"]
                    )[0]
                )
            set('general__site_hostname', options['hostname'])
        except Exception as e:
            print(e)


class PruneCommand(BaseCommand):
    help = "Remove domains and their related actors/objects. Might cause errors in the past federation relationships."

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument(
            'hostname',
            type=str,
            nargs='?',
            help="Domain hostname",
        )

    def handle(self, *args, **options):
        if options['hostname']:
            valid_domain(options['hostname'])
            Domain.objects.get_or_create(
                name=options["hostname"]
            )[0].delete()
            print("Domain {0} pruned".format(options['hostname']))
        else:
            for domain in Domain.objects.filter(actors=None).iterator():
                print("Domain {0} pruned".format(domain.name))
                domain.delete()


class Command(SubCommands):
    subcommands = {
        "migrate": MigrateCommand,
        "prune": PruneCommand
    }
