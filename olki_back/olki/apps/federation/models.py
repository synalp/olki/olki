# pylint: skip-file

import uuid
import logging
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from model_utils.managers import QueryManager

from django.apps import apps
from olki.apps.core.fields import AddFlagOneToOneField
from olki.apps.core.models import FederationMixin
from olki.apps.core.validators import DomainValidator
from . import utils as federation_utils

TYPE_CHOICES = [
    ("Person", "Person"),
    ("Tombstone", "Tombstone"),
    ("Application", "Application"),
    ("Group", "Group"),
    ("Organization", "Organization"),
    ("Service", "Service"),
]

logger = logging.getLogger(__name__)


def empty_dict():
    return {}


def get_shared_inbox_url():
    return federation_utils.full_url(reverse("federation:shared-inbox"))


class ActorQuerySet(models.QuerySet):
    def local(self, include=True):
        return (
            self.exclude(user__isnull=include)
            |
            self.filter(Q(preferred_username='olki') & Q(domain_id=settings.INSTANCE_HOSTNAME))
        )


class DomainQuerySet(models.QuerySet):
    def external(self):
        return self.exclude(pk=settings.INSTANCE_HOSTNAME)

    def with_actors_count(self):
        return self.annotate(actors_count=models.Count("actors", distinct=True))

    def with_outbox_activities_count(self):
        return self.annotate(
            outbox_activities_count=models.Count(
                "actors__outbox_activities", distinct=True
            )
        )


class Domain(models.Model):
    name = models.CharField(
        primary_key=True,
        max_length=255,
        validators=[DomainValidator()],
    )
    creation_date = models.DateTimeField(default=timezone.now)
    nodeinfo_fetch_date = models.DateTimeField(default=None, null=True, blank=True)
    nodeinfo = JSONField(default=empty_dict, max_length=50000, blank=True)
    service_actor = models.ForeignKey(
        "Actor",
        related_name="managed_domains",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    # are interactions with this domain allowed (only applies when allow-listing is on)
    allowed = models.BooleanField(default=None, null=True)

    objects = DomainQuerySet.as_manager()

    def __str__(self) -> str:
        return self.name

    def save(self, **kwargs):
        lowercase_fields = ["name"]
        for field in lowercase_fields:
            v = getattr(self, field, None)
            if v:
                setattr(self, field, v.lower())

        super().save(**kwargs)

    @property
    def is_local(self) -> bool:
        return self.name == settings.INSTANCE_HOSTNAME


class Actor(
    FederationMixin
):
    ap_type = "Actor"

    user = AddFlagOneToOneField(
        "account.User",
        related_name='actor',
        flag_name='has_actor',
        null=True,
        on_delete=models.CASCADE
    )
    outbox_url = models.URLField(max_length=500, null=True, blank=True)
    inbox_url = models.URLField(max_length=500)
    following_url = models.URLField(max_length=500, null=True, blank=True)
    followers_url = models.URLField(max_length=500, null=True, blank=True)
    shared_inbox_url = models.URLField(max_length=500, null=True, blank=True)
    type = models.CharField(choices=TYPE_CHOICES, default="Person", max_length=50)
    name = models.CharField(max_length=200, null=True, blank=True)
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, related_name="actors")
    summary = models.CharField(max_length=500, null=True, blank=True)
    preferred_username = models.CharField(max_length=200, null=True, blank=True)
    public_key = models.TextField(max_length=5000, null=True, blank=True)
    private_key = models.TextField(max_length=5000, null=True, blank=True)
    creation_date = models.DateTimeField(default=timezone.now)
    last_fetch_date = models.DateTimeField(default=timezone.now)
    manually_approves_followers = models.NullBooleanField(default=None)
    followers = models.ManyToManyField(
        to="self",
        symmetrical=False,
        through="Follow",
        through_fields=("target", "actor"),
        related_name="following",
    )
    icon = models.URLField(max_length=500, null=True, blank=True)
    image = models.URLField(max_length=500, null=True, blank=True)

    objects = ActorQuerySet.as_manager()

    class Meta:
        unique_together = ["domain", "preferred_username"]

    @property
    def webfinger_subject(self) -> str:
        return "{}@{}".format(self.preferred_username, settings.INSTANCE_HOSTNAME)

    @property
    def private_key_id(self) -> str:
        return "{}#main-key".format(self.fid)

    @property
    def profile_url(self) -> str:
        if self.is_local:
            return "{}/@{}".format(settings.INSTANCE_URL, self.username)
        else:
            return self.url or self.fid

    @property
    def username(self) -> str:
        return self.preferred_username

    @property
    def full_username(self) -> str:
        return "{}@{}".format(self.preferred_username, self.domain_id)

    def __str__(self) -> str:
        return "{}@{}".format(self.preferred_username, self.domain_id)

    def get_approved_followers(self):
        follows = self.received_follows.filter(approved=True)
        return self.followers.filter(pk__in=follows.values_list("actor", flat=True))

    @staticmethod
    def should_autoapprove_follow(actor) -> bool:
        return True if not actor.manually_approves_followers else bool(actor.manually_approves_followers)

    def get_user(self):
        try:
            return self.user
        except ObjectDoesNotExist:
            return None

    @property
    def keys(self):
        return self.private_key, self.public_key

    @keys.setter
    def keys(self, v):
        self.private_key = v[0].decode("utf-8")
        self.public_key = v[1].decode("utf-8")

    def migrate_domain(self, domain=None):
        actor_data = self.get_actor_data(self.username)
        for attr, value in actor_data.items():
            setattr(self, attr, value)
        self.domain = domain or Domain.objects.get_or_create(
            name=settings.INSTANCE_HOSTNAME
        )[0]
        self.save()
        return self

    def get_fid(self):
        """
        used at init by FederationMixin to set self.fid (as it is used in queries) but still the best way to display the fid
        :return: string
        """
        if self.is_local:
            return self.get_actor_data(self.preferred_username)["fid"]
        return self.fid

    @staticmethod
    def get_actor_data(username):
        slugified_username = federation_utils.slugify_username(username)
        return {
            "preferred_username": slugified_username,
            "domain": Domain.objects.get_or_create(
                name=settings.INSTANCE_HOSTNAME
            )[0],
            "type": "Person",
            "name": username,
            "manually_approves_followers": False,
            "fid": federation_utils.full_url(
                reverse(
                    "federation:actor-detail",
                    kwargs={"preferred_username": slugified_username},
                )
            ),
            "url": federation_utils.full_url(
                reverse(
                    "profile",
                    kwargs={"user_id": slugified_username},
                    urlconf="olki.apps.account.spa_urls",
                )
            ),
            "shared_inbox_url": get_shared_inbox_url(),
            "inbox_url": federation_utils.full_url(
                reverse(
                    "federation:actor-inbox",
                    kwargs={"preferred_username": slugified_username},
                )
            ),
            "outbox_url": federation_utils.full_url(
                reverse(
                    "federation:actor-outbox",
                    kwargs={"preferred_username": slugified_username},
                )
            ),
            "followers_url": federation_utils.full_url(
                reverse(
                    "federation:actor-followers",
                    kwargs={"preferred_username": slugified_username},
                )
            ),
            "following_url": federation_utils.full_url(
                reverse(
                    "federation:actor-following",
                    kwargs={"preferred_username": slugified_username},
                )
            ),
        }

    def is_instance_actor(self) -> bool:
        return self.preferred_username == 'olki' and self.domain.is_local

    def scan(self, by_actor=None):
        if self.is_local:
            return  # we don't scan local actors

        last_seen_activity = Activity.objects.filter(actor=self).last()
        last_seen_id = None
        if last_seen_activity:
            last_seen_id = last_seen_activity.fid

        activities = federation_utils.retrieve_activities_in_collection(
            self.outbox_url,
            filter_activity_type_in=['Create', 'Update', 'Delete'],
            filter_object_type_in=['Corpus'],
            last_seen_id=last_seen_id
        )

        from . import activity

        for activity_data in activities:
            activity.receive(activity_data, on_behalf_of=self)


FETCH_STATUSES = [
    ("pending", "Pending"),
    ("errored", "Errored"),
    ("finished", "Finished"),
    ("skipped", "Skipped"),
]


class FetchQuerySet(models.QuerySet):
    def get_for_object(self, object):
        content_type = ContentType.objects.get_for_model(object)
        return self.filter(object_content_type=content_type, object_id=object.pk)


class Fetch(models.Model):
    url = models.URLField(max_length=500, db_index=True)
    creation_date = models.DateTimeField(default=timezone.now)
    fetch_date = models.DateTimeField(null=True, blank=True)
    object_id = models.IntegerField(null=True)
    object_content_type = models.ForeignKey(
        ContentType, null=True, on_delete=models.CASCADE
    )
    object = GenericForeignKey("object_content_type", "object_id")
    status = models.CharField(default="pending", choices=FETCH_STATUSES, max_length=20)
    detail = JSONField(
        default=empty_dict, max_length=50000, encoder=DjangoJSONEncoder, blank=True
    )
    actor = models.ForeignKey(Actor, related_name="fetches", on_delete=models.CASCADE)

    objects = FetchQuerySet.as_manager()

    def save(self, **kwargs):
        if not self.url and self.object:
            self.url = self.object.fid

        super().save(**kwargs)

    @property
    def serializers(self):
        return {
            # contexts.AS.Note: serializers.NoteSerializer,
            # contexts.AS.Audio: serializers.UploadSerializer,
        }


class InboxItem(models.Model):
    """
    Store activities binding to local actors, with read/unread status.
    """

    actor = models.ForeignKey(
        Actor, related_name="inbox_items", on_delete=models.CASCADE
    )
    activity = models.ForeignKey(
        "Activity", related_name="inbox_items", on_delete=models.CASCADE
    )
    type = models.CharField(max_length=10, choices=[("to", "to"), ("cc", "cc")])
    is_read = models.BooleanField(default=False)

    class InboxItemManager(models.Manager):
        def bulk_create(self, objs, **kwargs):
            bulk = super().bulk_create(objs, **kwargs)
            for i in objs:
                post_save.send(i.__class__, instance=i, created=True)
            return bulk

    objects = InboxItemManager()

    class Meta:
        # unread first, then most recent first
        ordering = ['-id']


class Delivery(models.Model):
    """
    Store deliveries attempt to remote inboxes
    """

    is_delivered = models.BooleanField(default=False)
    last_attempt_date = models.DateTimeField(null=True, blank=True)
    attempts = models.PositiveIntegerField(default=0)
    inbox_url = models.URLField(max_length=500)

    activity = models.ForeignKey(
        "Activity", related_name="deliveries", on_delete=models.CASCADE
    )


class Activity(
    FederationMixin
):
    """
    Activities store references to the objects and targets. Their payload
    is what gets sent on the network and is the result of processing other
    attributes through the dispatched route that created the Activity.
    """
    actor = models.ForeignKey(
        Actor, related_name="outbox_activities", on_delete=models.CASCADE
    )
    recipients = models.ManyToManyField(
        Actor, related_name="inbox_activities", through=InboxItem
    )
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    payload = JSONField(default=empty_dict, max_length=50000, encoder=DjangoJSONEncoder)
    creation_date = models.DateTimeField(default=timezone.now, db_index=True)
    type = models.CharField(db_index=True, null=True, max_length=100)

    # generic relations
    object_id = models.CharField(null=True, max_length=16)
    object_content_type = models.ForeignKey(
        ContentType,
        null=True,
        on_delete=models.SET_NULL,
        related_name="objecting_activities",
    )
    object = GenericForeignKey("object_content_type", "object_id")
    target_id = models.CharField(null=True, max_length=16)
    target_content_type = models.ForeignKey(
        ContentType,
        null=True,
        on_delete=models.SET_NULL,
        related_name="targeting_activities",
    )
    target = GenericForeignKey("target_content_type", "target_id")
    related_object_id = models.CharField(null=True, max_length=16)
    related_object_content_type = models.ForeignKey(
        ContentType,
        null=True,
        on_delete=models.SET_NULL,
        related_name="related_objecting_activities",
    )
    related_object = GenericForeignKey(
        "related_object_content_type", "related_object_id"
    )

    class ActivityManager(models.Manager):
        def bulk_create(self, objs, **kwargs):
            bulk = super().bulk_create(objs, **kwargs)
            for i in objs:
                post_save.send(i.__class__, instance=i, created=True)
            return bulk

    objects = ActivityManager()
    # Some notable shortcuts to common filtering cases:
    Corpus = QueryManager(Q(payload__object__type='Corpus') & Q(object_id__isnull=False))
    Note = QueryManager(Q(payload__object__type='Note') & Q(object_id__isnull=False))

    local_filter = Q(actor__domain__name=settings.INSTANCE_HOSTNAME)
    Local = QueryManager(local_filter)
    Remote = QueryManager(~local_filter)


@receiver(post_save, sender=Activity, dispatch_uid="save_id_for_activity_payload")
def save_id_for_activity_payload(sender, instance, created, **kwargs):
    if created:
        if instance.is_local:
            instance.payload['id'] = settings.INSTANCE_URL + '/activity/{}'.format(instance.uuid)
            instance.save()


class AbstractFollow(models.Model):
    ap_type = "Follow"

    fid = models.URLField(unique=True, max_length=500, null=True, blank=True)
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    creation_date = models.DateTimeField(default=timezone.now)
    modification_date = models.DateTimeField(auto_now=True)
    approved = models.NullBooleanField(default=None)

    class Meta:
        abstract = True

    def get_federation_id(self) -> str:
        return federation_utils.full_url(
            "{}#follows/{}".format(self.actor.get_fid(), self.uuid)
        )


class Follow(AbstractFollow):
    actor = models.ForeignKey(
        Actor, related_name="emitted_follows", on_delete=models.CASCADE
    )
    target = models.ForeignKey(
        Actor, related_name="received_follows", on_delete=models.CASCADE
    )

    class Meta:
        unique_together = ["actor", "target"]
