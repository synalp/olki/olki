## OLKi backend settings

You can fine-tune settings and literally override every Django setting
by creating a `backend_setting.py` in the root directory, and setting variables
listed in `components/` or supported by the `INSTALLED_APPS` modules.

The most common variables are usually exposed for modification in your
root `.env`, so only use a backend settings file in specific cases where
you know what you are doing.

### Editing components

Components in `components/` are split by scope. General settings can be
found in `components/settings.py`, as well with settings that are not
sorted in a separate scope yet.
