import persisting_theory


class FactoriesRegistry(persisting_theory.Registry):
    look_into = "factories"

    def prepare_name(self, data, name=None):
        return name or data._meta.model._meta.label


class NoUpdateOnCreate:
    """
    Factory boy calls save after the initial create. In most case, this
    is not needed, so we disable this behaviour
    """

    @classmethod
    def _after_postgeneration(cls, instance, create, results=None):
        return


registry = FactoriesRegistry()
