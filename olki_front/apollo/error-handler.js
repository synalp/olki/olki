export default async (
  { graphQLErrors, networkError, operation, forward },
  nuxtContext
) => {
  console.log(`[GraphQL Error]: `, graphQLErrors, operation, forward)
  await nuxtContext.app.apolloProvider.defaultClient.queryManager
    .fetchQueryRejectFns
  await nuxtContext.app.apolloProvider.defaultClient.resetStore()

  if (networkError) {
    console.log(`[Network error]: ${networkError}`)
  }
}
