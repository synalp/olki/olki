export default context => {
  return {
    httpEndpoint: context.env.baseURL + '/graphql'
  }
}
