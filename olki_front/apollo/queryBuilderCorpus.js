import { pick } from '~/utilities/objects'

export const baseQueryVariables = {
  first: 15,
  excludeDrafts: true,
  excludeQuarantined: true
}

export const buildQuery = (query, variables) => {
  return {
    query,
    variables: { ...baseQueryVariables, ...variables },
    fetchPolicy: 'network-only',
    debounce: 300, // ms
    update: r => r,
    watchLoading(isLoading, countModifier) {
      countModifier === 1
        ? this.$nextTick(() => {
            this.$nuxt.$loading.start()
          })
        : this.$nextTick(() => {
            this.$nuxt.$loading.finish()
          })
    }
  }
}

export const parseCorpus = corpus => {
  try {
    if (
      corpus.tags &&
      Object.prototype.toString.call(corpus.tags) === '[object String]'
    )
      corpus.tags = JSON.parse(corpus.tags)
    if (
      corpus.related &&
      Object.prototype.toString.call(corpus.related) === '[object String]'
    )
      corpus.related = JSON.parse(corpus.related)
  } catch (e) {
    // not yielding anything yet
  }
  return corpus
}

export const parseSearchTags = searchTags => {
  try {
    if (Object.prototype.toString.call(searchTags) === '[object String]')
      searchTags = JSON.parse(searchTags)
    if (Object.prototype.toString.call(searchTags) !== '[object Array]')
      searchTags = [searchTags]
    searchTags = searchTags
      .map(t => {
        return Object.prototype.toString.call(t) === '[object String]'
          ? JSON.parse(t)
          : t
      })
      .map(t => pick(t, ['name', 'type', 'filtervalue']))
  } catch (e) {
    // not yielding anything yet
  }
  return searchTags
}
