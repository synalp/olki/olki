export default {
  data() {
    return {
      baseURL: process.env.baseURL,
      isDev: process.env.NODE_ENV !== 'production'
    }
  }
}
