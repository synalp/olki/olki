export const state = () => ({
  data: {
    primary: '#205e3b'
  }
})

export const mutations = {
  set(state, data) {
    state.data = data
  },
  setPrimary(state, data) {
    state.data.primary = data
  }
}

export const getters = {
  get: (state, getters) => {
    return state.data
  }
}
