export const state = () => ({
  data: {},
  showNewcomerWelcome: true
})

export const mutations = {
  set(state, data) {
    state.data = data
  },
  dismissNewcomerWelcome(state) {
    state.showNewcomerWelcome = false
  },
  resetDismissableHelpers(state) {
    state.showNewcomerWelcome = true
  }
}

export const getters = {
  getOpenRegistrations(state) {
    return state.data.openRegistrations
  },
  getTerms(state) {
    return state.data.metadata ? state.data.metadata.nodeTerms : ''
  },
  getRules(state) {
    return state.data.metadata ? state.data.metadata.nodeRules : ''
  },
  getInstanceName(state) {
    return state.data.metadata
      ? state.data.metadata.nodeName
        ? state.data.metadata.nodeName
        : 'OLKi'
      : 'OLKi'
  },
  getInstanceDescription(state) {
    return state.data.metadata ? state.data.metadata.nodeDescription : ''
  },
  getInstanceSubjects(state) {
    return state.data.metadata ? state.data.metadata.nodeSubjects : ''
  },
  getInstanceVersion(state) {
    return state.data.software ? state.data.software.version : ''
  }
}

export const actions = {
  async syncNodeinfo({ commit }) {
    try {
      const res = await this.$axios.$get('.well-known/nodeinfo/2.1')
      commit('set', res)
    } catch (e) {
      console.log(e)
    }
  }
}
