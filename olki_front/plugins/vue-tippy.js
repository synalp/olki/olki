/* eslint-disable */
// there is a multiple import below but it seems unavoidable
import Vue from 'vue'
import VueTippy from 'vue-tippy'
import { TippyComponent } from 'vue-tippy'

Vue.use(VueTippy, {
  size: 'small',
  placement: 'bottom',
  theme: 'olki'
})
Vue.component('tippy', TippyComponent)
