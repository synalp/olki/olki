import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  window.onNuxtReady(() => {
    createPersistedState({
      key: 'olki',
      paths: ['auth', 'theme']
    })(store)
  })
}
