import Vue from 'vue'
import Zutre from 'zutre'

export default () => {
  Vue.use(Zutre)
}
