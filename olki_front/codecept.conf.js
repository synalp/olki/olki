exports.config = {
  tests: 'test/browser/*_test.js',
  output: 'test/browser-reports/',
  helpers: {
    Nightmare: {
      url: 'http://localhost:3000',
      show: false,
      restart: false,
      typeInterval: -1,
      windowSize: '1440x1024',
      waitforTimeout: 1000
    }
  },
  bootstrap: null,
  mocha: {},
  name: 'olki_front',
  plugins: {
    allure: {},
    stepByStepReport: {
      enabled: true
    },
    autoLogin: {
      inject: 'login',
      users: {
        admin: {
          login: I => {
            I.amOnPage('/about?to=login')
            I.fillField('email', 'superuser@example.com')
            I.fillField('password', 'test')
            I.click('Log in')
          },
          check: I => {
            I.amOnPage('/')
            I.see('Profile')
          },
          fetch: () => {}, // empty function
          restore: () => {} // empty funciton
        }
      }
    }
  }
}
