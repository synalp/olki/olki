import Vuex from 'vuex'

/*
const moduleA = {
  state: { ... },
  mutations: { ... },
  actions: { ... },
  getters: { ... }
}
const modules = {moduleA}
 */
export function createStore(modules) {
  return new Vuex.Store({
    modules: modules
  })
}
