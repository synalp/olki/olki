import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import test from 'ava'
import sinon from 'sinon'
import Vuex from 'vuex'
import Zutre from 'zutre'
import Vue2Filters from 'vue2-filters'
import { createStore } from '../helpers/utils'
import Component from '../../components/Heading.vue'

const localVue = createLocalVue()
localVue.use(Zutre)
localVue.use(Vue2Filters)
localVue.use(Vuex)

let store
let wrapper

/**
 * Mock the store before each test.
 */
test.beforeEach(t => {
  store = createStore({
    instance: {
      namespaced: true,
      state: {},
      mutations: {},
      actions: {},
      getters: {
        getInstanceName: sinon.spy()
      }
    },
    user: {
      namespaced: true,
      state: {},
      mutations: {},
      actions: {},
      getters: {
        isAdministrator: sinon.spy(),
        isModerator: sinon.spy()
      }
    },
    auth: {
      namespaced: true,
      state: {
        isAuthenticated: sinon.spy()
      },
      mutations: {},
      actions: {},
      getters: {}
    }
  })
  wrapper = shallowMount(Component, {
    localVue,
    store,
    mocks: {
      $t: key => key,
      localePath: key => key
    },
    stubs: {
      NuxtLink: RouterLinkStub
    }
  })
})

test('Heading.vue snap shot', t => {
  t.snapshot({ html: wrapper.html() })
})

test('has a default instance subtitle', t => {
  const subtitle = wrapper.find('.subtitle')

  t.true(subtitle.text() === 'Federated Knowledge')
})

test('has no avatar heading when unauthenticated', t => {
  const avatarHeading = wrapper.find('avatar-heading')

  t.false(avatarHeading.exists())
})
