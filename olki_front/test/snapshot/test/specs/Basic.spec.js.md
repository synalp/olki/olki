# Snapshot report for `test/specs/Basic.spec.js`

The actual snapshot is saved in `Basic.spec.js.snap`.

Generated by [AVA](https://ava.li).

## Heading.vue snap shot

> Snapshot 1

    {
      html: '<z-navbar-stub><z-navbar-section-stub><ul class="tab tab-block ml-2"><li class="tab-item"><a>browse</a></li><li class="tab-item"><a>social</a></li><action-select-stub class="ml-2"></action-select-stub></ul><ul class="tab tab-block"></ul></z-navbar-section-stub><z-navbar-section-stub center="true"><a class="title">OLKi<p class="subtitle">Federated Knowledge</p></a></z-navbar-section-stub><z-navbar-section-stub><rapid-search-stub class="mr-2"></rapid-search-stub><language-select-stub class="mr-2"></language-select-stub><div><avatar-heading-stub></avatar-heading-stub></div></z-navbar-section-stub></z-navbar-stub>',
    }
