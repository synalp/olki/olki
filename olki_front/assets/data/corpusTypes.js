/* eslint-disable */
const types = [
  { code: 'wikidata.org/entity/Q10870555', name: 'Report', description: 'Formal, detailed text' },
  { code: 'wikidata.org/entity/Q3284399', name: 'Statistical Model', description: 'Type of mathematical model' },
  { code: 'wikidata.org/entity/Q187685', name: 'Thesis', description: 'Document submitted in support of candidature for a doctorate' },
  { code: 'wikidata.org/entity/Q580922', name: 'Preprint', description: 'Version of a scholarly or scientific paper that precedes publication in a peer-reviewed scholarly or scientific journal' },
  { code: 'wikidata.org/entity/Q23927052', name: 'Conference paper', description: 'Document published as part of the proceedings of an academic conference' },
  { code: 'wikidata.org/entity/Q571', name: 'Book', description: 'Medium for a collection of words and/or pictures to represent knowledge or a fictional story' },
  { code: 'wikidata.org/entity/Q324254', name: 'Ontology', description: 'Specification of a conceptualization' },
  { code: 'wikidata.org/entity/Q603773', name: 'Lecture/Presentation', description: 'Oral presentation intended to present information or teach people about a particular subject' },
  { code: 'wikidata.org/entity/Q128751', name: 'Source Code', description: 'Collection of computer instructions written using some human-readable computer language' },
  { code: 'wikidata.org/entity/Q379833', name: 'Lesson', description: 'Section of learning or teaching into which a wider learning content is divided' },
  { code: 'wikidata.org/entity/Q478798', name: 'Image(s)', description: 'Artifact that depicts or records visual perception' },
  { code: 'wikidata.org/entity/Q26987250', name: 'Audio', description: 'Relating to sound waves that have been electronically recorded, transmitted, or reproduced' }
]

export default types
