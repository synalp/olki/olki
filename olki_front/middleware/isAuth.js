export default function({ app, error }) {
  const hasToken = !!app.$apolloHelpers.getToken()
  if (!hasToken) {
    error({ statusCode: 503, message: 'You are not allowed to see this' })
  }
}
