#!/bin/bash -eux
pushd olki_back
poetry run python manage.py migrate
poetry run python manage.py -- collectstatic --noinput
poetry run python manage.py collecttemplates
popd
poetry run honcho start
