#!/bin/bash

version=$1

poetry version "$1"
sed -i "/__version__/s/.*/__version__ = '$version'/" olki_back/olki/__init__.py
