#!/bin/bash

name=$1
version=$2

rm -rf ./dist
mkdir -p dist
git archive --worktree-attributes --format tar --prefix ${name}-${version}/ develop | xz -9ve > dist/${name}-${version}.tar.xz
gpg2 --output dist/${name}-${version}.tar.xz.asc --digest-algo SHA512 --armor --detach-sig dist/${name}-${version}.tar.xz

make build-fe
tar cf - olki_front/dist/ | xz -z - > dist/${name}-${version}-frontend.tar.xz
gpg2 --output dist/${name}-${version}-frontend.tar.xz.asc --digest-algo SHA512 --armor --detach-sig dist/${name}-${version}-frontend.tar.xz
