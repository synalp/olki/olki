include:
  - local: /support/gitlab-ci/docker.gitlab-ci.yml
  - local: /support/gitlab-ci/deploy.gitlab-ci.yml

###
## We use a custom python:3.7-stretch image
## defined in support/docker/Dockerfile.ci
## to speedup the dependency installation.
##
## Check the custom image Dockerfile to see
## which assumptions are taken (for poetry
## and depedencies installed on Debian).
#
image: rigelk/olki-ci

stages:
  - lint
  - test
  - build
  - publish
  - deploy

variables:
  # used in backend tests
  POSTGRES_DB: test_olki
  POSTGRES_USER: olki
  POSTGRES_PASSWORD: ""
  POSTGRES_HOST: postgres
  POSTGRES_HOST_AUTH_METHOD: trust
  # used in docker build/push
  REGISTRY_IMAGE: rigelk/olki # name of the production image
  BUILD_METHOD: 'dind'

###
## Job templates
#

.lints:
  stage: lint
  except:
    - schedules

.tests:
  stage: test
  except:
    - schedules

###
## Lint
#

lint backend:
  extends: .lints
  before_script:
    # Switching to the cached virtualenv brought by the docker image
    - source /app/.venv/bin/activate
    # Dependencies actualisation
    - make deps-dev-be
  script:
    - poetry run prospector olki_back -P olki_back/.prospector.yaml
    - poetry run pylint --rcfile=olki_back/pylintrc --output-format=text olki_back
  when: manual

lint fsfe/reuse compliance:
  extends: .lints
  image: fsfe/reuse:latest
  script:
    - reuse lint
  only:
    changes:
      - "LICENSES/*"
      - ".reuse/*"
  allow_failure: true

###
## Test
#

test backend:
  extends: .tests
  services:
    - postgres:12.2-alpine
    - redis
  variables:
    # Note that any variable passed down to tox must be whitelisted
    # in `olki_back/setup.cfg`'s passenv. Hence using a CI dotfile.
    DOTFILE: '.env.ci'
  before_script:
    # Switching to the cached virtualenv brought by the docker image
    - source /app/.venv/bin/activate
    # Dependencies actualisation
    - make deps-dev-be
    # Database migration
    - make manage -- migrate
  script:
    - make test-be-pytest-only

test frontend:
  extends: .tests
  image: node:current
  before_script:
    - make deps-dev-fe
  script:
    - make test-fe

test coverage:
  extends: .tests
  before_script:
    # Switching to the cached virtualenv brought by the docker image
    - source /app/.venv/bin/activate
    # Dependencies actualisation
    - make deps-dev-be
  script:
    - poetry run coverage run --source=olki ./olki_back/manage.py test
    - poetry run coverage report -m
  when: manual

###
## Build
#

build frontend:
  image: node:current
  stage: build
  before_script:
    - make deps-dev-fe
  script:
    - make build-fe
  artifacts:
    expire_in: 1 day
    paths:
      - olki_front/dist/
  only:
    - tags@synalp/olki/olki
    - develop@synalp/olki/olki

