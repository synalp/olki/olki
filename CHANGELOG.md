# Changelog

All notable changes to this project will be documented in this file.

The format is loosely based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.0-beta.0](https://framagit.org/synalp/olki/compare/0.3.0-alpha.0...0.4.0-beta.0) - 2020-05-20

Fourth release for wider beta testing, with Corpus federation and instance follows.

### Features

- instance follows, with an instance actor (olki@domain)
- initial corpus federation: create, update, deletion with disabled comments on remote instance until further testing
- comments can be commented on from third-party software, and will see more focus in later versions

### Security

- inbox now requires correct signature on GET

## [0.3.0-alpha.0](https://framagit.org/synalp/olki/compare/0.2.0-alpha.0...0.3.0-alpha.0) - 2020-03-22

Third release for demonstration before wider beta testing, with Note (comments) federation with Mastodon.

### Fixed

- fixing createCorpus bug and adding more corpus test [`#26`](https://framagit.org/synalp/olki/issues/26)

### Features

- switching to asgi, channels and subscriptions for notifications [`ef6bf09`](https://framagit.org/synalp/olki/commit/ef6bf0960b05980e603682786a28a7d573af5245)
- adding system notification system and UI [`73b6d8e`](https://framagit.org/synalp/olki/commit/73b6d8e13f04c8929b60ace8504b8884c7362be0)
- adding follows command and development info card [`1be1b54`](https://framagit.org/synalp/olki/commit/1be1b547ec8a8c921759924fee0c250fd98c4b4e)

## [0.2.0-alpha.0](https://framagit.org/synalp/olki/compare/0.1.0-alpha.0...0.2.0-alpha.0) - 2019-10-07

Second release for internal tests.

### Features

- initial ederation code [`1c1f8c7`](https://framagit.org/synalp/olki/commit/1c1f8c7992f6ded4592ddbdda0caa6a671823e37)
- using FSFE Reuse standard and achieving compliance [`24b4db8`](https://framagit.org/synalp/olki/commit/24b4db8413e3923ecd0d845389903ad80d23845e)
- adding graphql config and schema [`090627d`](https://framagit.org/synalp/olki/commit/090627d202d6f5d89b81b3cb78e156fd8de26e7c)

## 0.1.0-alpha.0 - 2019-06-17

Initial __alpha__ release for the OLKi platform. API and database are both changing - this release is meant for demonstration.

### Features

- initial orpora layout and model, admin user list and instance settings [`#2`](https://framagit.org/synalp/olki/issues/2) [`#6`](https://framagit.org/synalp/olki/issues/6)
- register and profile update for local users [`#1`](https://framagit.org/synalp/olki/issues/1)
- corpus upload form, user theme [`4c176b4`](https://framagit.org/synalp/olki/commit/4c176b4ed19a7fe9b7f32eb7b7e168b235c9c532)
- moving from sapperjs used in demonstrator to nuxtjs [`593ecc3`](https://framagit.org/synalp/olki/commit/593ecc34050cd99985ecc7ffe531571f6ee3bd4e)
- initial commit [`2157809`](https://framagit.org/synalp/olki/commit/2157809cbc63c127a62247195afdf9749e0ebd33)
